
## 💻 Projeto

Tecnologias utilizadas:

- React:
ReactHooks para gerenciamento de estados;

- Bootstrap:
Reactstrap para criação de componentes responsivos;

- HTML | CSS | Javascript

- API consultada via mock em .json (express.js removido)

## 🤔 Como inicializar

- npm install > para baixar as dependências;
- npm start > para rodar o projeto.


