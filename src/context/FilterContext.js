import React, { createContext, useState, useContext } from 'react';

//Creating context to share the selected items on the header/side menu to the Main.jsx file
const FilterContext = createContext();

export default function FilterProvider({ children }) {
    const [clicked, setClicked] = useState(0);
    const [clickedPants, setClickedPants] = useState(0);
    const [clickedShoes, setClickedShoes] = useState(0);

    return (
        <FilterContext.Provider
            value={{ clicked, setClicked, clickedPants, setClickedPants, clickedShoes, setClickedShoes }}>
            {children}
        </FilterContext.Provider>
    );
}

export function useFilter() {
    const context = useContext(FilterContext);
    const { clicked, setClicked, clickedPants, setClickedPants, clickedShoes, setClickedShoes } = context;
    return { clicked, setClicked, clickedPants, setClickedPants, clickedShoes, setClickedShoes };
}