import React, { useState, useContext } from 'react';
import '../components/Header.css';
import '../../src/globalstyles.css';
import logo from '../assets/logo.png';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';
import { useFilter } from '../context/FilterContext';

function Header() {

    //Toggle for responsive menu
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);
    //Using context to filter products when clicked
    const { clicked, setClicked, clickedPants, setClickedPants, clickedShoes, setClickedShoes } = useFilter();

    return (
        <>
            <header className="main-header">
                <div className="preHeader">
                    <div className="preHeader-login">
                        <a href="#">Acesse sua conta</a><span>ou</span><a href="#">Cadastre-se</a>
                    </div>
                </div>
                <div className="header">
                    <img className="logo" src={logo} alt="WEBJUMP" />
                    <div className="search-wrap">
                        <div className="search">
                            <input type="text" className="searchTerm" id="searchTerm" />
                            <button type="submit" className="searchButton"> Buscar </button>
                            <i className="fa fa-search"></i>
                        </div>
                    </div>
                </div>
                <nav className="global-nav">
                    <Navbar color="light" light expand="md">
                        <NavbarToggler onClick={toggle} />
                        <Collapse className="collapse" isOpen={isOpen} navbar>
                            <Nav className="mr-auto" navbar>
                                <NavItem>
                                    <NavLink href="/">Página Inicial</NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink>
                                        <a className="headerItems" onClick={() => setClicked(clicked + 1)}> Camisetas </a> </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink>
                                        <a className="headerItems" onClick={() => setClickedPants(clickedPants + 1)}> Calças </a> </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink>
                                        <a className="headerItems" onClick={() => setClickedShoes(clickedShoes + 1)}> Sapatos </a> </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink href="#">Contato</NavLink>
                                </NavItem>
                                <NavItem className="hide">
                                    <NavLink href="#">Acesse</NavLink>
                                </NavItem>
                                <NavItem className="hide">
                                    <NavLink href="#">Cadastre-se</NavLink>
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </Navbar>
                </nav>
            </header>
        </>
    );
}

export default Header;