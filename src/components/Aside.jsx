import React from 'react';
import '../components/Aside.css';
import '../../src/globalstyles.css';
import { useFilter } from '../context/FilterContext';

function Aside() {
    //Using context to filter products when clicked
    const { clicked, setClicked, clickedPants, setClickedPants, clickedShoes, setClickedShoes } = useFilter();

    return (
        <>
            <aside>
                <div className="aside-wrapper">
                    <div className="aside-border">
                        <div className="aside-title">Filtre por</div>
                        <div className="aside-categories">Categorias</div>
                        <ul className="aside-list">
                            <li><a href="#" onClick={() => setClicked(clicked + 1)}> Camisetas</a></li>
                            <li><a href="#" onClick={() => setClickedPants(clickedPants + 1)}> Calças</a></li>
                            <li><a href="#" onClick={() => setClickedShoes(clickedShoes + 1)}> Sapatos</a></li>
                        </ul>
                        <div className="aside-categories">Cores</div>
                        <ul>
                            <li>
                                <button className="aside-button1">.</button>
                                <button className="aside-button2">.</button>
                                <button className="aside-button3">.</button>
                            </li>
                        </ul>
                        <div className="aside-categories">Tipo</div>
                        <ul className="aside-list">
                            <li><a href="#">Corrida</a></li>
                            <li><a href="#">Caminhada</a></li>
                            <li><a href="#">Casual</a></li>
                            <li><a href="#">Social</a></li>
                        </ul>
                    </div>
                </div>
            </aside>
        </>
    );
}

export default Aside;