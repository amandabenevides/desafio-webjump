import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import '../components/Main.css';
import '../../src/globalstyles.css';
import * as api1 from '../assets/mock-api/V1/categories/1.json';
import * as api2 from '../assets/mock-api/V1/categories/2.json';
import * as api3 from '../assets/mock-api/V1/categories/3.json';
import { useFilter } from '../context/FilterContext';

function Main() {

    const [products, setProducts] = useState([]);
    const [productsApi2, setProductsApi2] = useState([]);
    const [productsApi3, setProductsApi3] = useState([]);
    const [search, setSearch] = useState('');
    const { clicked, setClicked, clickedPants, setClickedPants, clickedShoes, setClickedShoes } = useFilter();

    // Updating the products list
    useEffect(() => {
        setProducts(arrayFormat);
        setProductsApi2(arrayFormatApi2);
        setProductsApi3(arrayFormatApi3);
    }, []);

    //Formatting the .json files
    let array = Object.keys(api1).map(key => api1[key]);
    let arrayFormat = array[0].items;

    let arrayApi2 = Object.keys(api2).map(key => api2[key]);
    let arrayFormatApi2 = arrayApi2[0].items;

    let arrayApi3 = Object.keys(api3).map(key => api3[key]);
    let arrayFormatApi3 = arrayApi3[0].items;

    //Checking if the items on the APIs match the search bar input
    let filteredItemsApi1 = products.filter(item => {
        return item.name.toLowerCase().includes(search.toLowerCase())
    })

    const filteredItemsApi2 = productsApi2.filter(item => {
        return item.name.toLowerCase().includes(search.toLowerCase())
    })

    const filteredItemsApi3 = productsApi3.filter(item => {
        return item.name.toLowerCase().includes(search.toLowerCase())
    })

    // Updating the search field according to the title selected on the header/side menu
    useEffect(() => {
        if (clicked) {
            setSearch("camiseta");
            setClicked(0);
        }
        if (clickedPants) {
            setSearch("calça");
            setClickedPants(0);
        }
        if (clickedShoes) {
            setSearch("tênis");
            setClickedShoes(0);
        }
    });

    return (
        <>
            <main className="main">
                <div className="content">
                    <div>
                        <Container>
                            <Row>
                                <div className="products-title"><span className="span-title">Produtos</span>
                                    <input className="search-product-bar" type="text" placeholder="Buscar" onChange={e => setSearch(e.target.value)} />
                                </div>
                            </Row>
                        </Container>
                        <Container>
                            <Row>
                                {filteredItemsApi1.map((item) => (
                                    <>
                                        <Col md={3}>
                                            <div className="item">
                                                <div className="product-image">
                                                    <img className="p-img" key={item.id} src={item.image} alt="product image"></img></div>
                                                <div className="product-name" id="productName">{item.name} </div>
                                                <div className="product-price">R${item.price.toFixed(2).replace('.', ',')}</div>
                                                <button className="buy-button">Comprar</button>
                                            </div>
                                        </Col>
                                    </>
                                ))}

                                {filteredItemsApi2.map((item) => (
                                    <>
                                        <Col md={3}>
                                            <div className="item">
                                                <div className="product-image">
                                                    <img className="p-img" key={item.id} src={item.image} alt="product image"></img></div>
                                                <div className="product-name">{item.name} </div>
                                                <div className="product-price">R${item.price.toFixed(2).replace('.', ',')}</div>
                                                <button className="buy-button">Comprar</button>
                                            </div>
                                        </Col>
                                    </>
                                ))}

                                {filteredItemsApi3.map((item) => (
                                    <>
                                        <Col md={3}>
                                            <div className="item">
                                                <div className="product-image">
                                                    <img className="p-img " key={item.id} src={item.image} alt="product image"></img></div>
                                                <div className="product-name">{item.name} </div>
                                                <div className="product-price">R${item.price.toFixed(2).replace('.', ',')}</div>
                                                <button className="buy-button">Comprar</button>
                                            </div>
                                        </Col>
                                    </>
                                ))}
                            </Row>
                        </Container>
                    </div>
                </div>
            </main>
        </>
    );
}

export default Main;
