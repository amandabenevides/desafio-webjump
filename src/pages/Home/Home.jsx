import React, { useContext } from 'react';
import Header from '../../components/Header.jsx';
import Aside from '../../components/Aside.jsx';
import Main from '../../components/Main.jsx';
import '../../globalstyles.css';
import { Row, Col } from 'reactstrap';
import FilterProvider from '../../context/FilterContext';


export default function Home() {

    return (
        <>
            <FilterProvider>
                <Header />
                <Row>
                    <Col sm={3}>
                        <Row>
                            <Aside />
                        </Row>
                    </Col>
                    <Col sm={9}>
                        <Row>
                            <Main />
                        </Row>
                    </Col>
                </Row>
                <footer className="footer" />
            </FilterProvider>
        </>
    );
}